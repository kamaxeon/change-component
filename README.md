# change-component

This program runs a webservice on port 4243, where you can mass
change your JIRA issues to a specific kernel/kernel-rt comonent.

## Running

### Build the container

podman build . -t change-component-0.01

### Run the container

podman run -it -p 4243:4243/tcp  localhost/change-component-0.01

## License

MIT.


